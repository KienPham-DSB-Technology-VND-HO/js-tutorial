## setTimeout có phải promise hay không ?
- setTimeout xử lý bất đồng bộ
- setTimeout không phải là promise và không có thể được resolve hoặc reject. Nó chỉ thực thi một lần và sau đó mới bị hủy.Cũng như trì hoãn một chức năng với một thời gian được xác định.
- setTimeout có thể được gọi nhiều lần, nhưng nó chỉ chạy một lần.
- Nhận vào 2 thâm số là callback, time. 

***
## Promise
  - được sử dụng để xử lý các hoạt động không đồng bộ. Cung cấp một cách tiếp cận thay thế callback bằng cách callback hell và dễ dàng viết mã hơn.
  - Promise là một cách để thực thi code một cách bất đồng bộ. Nó cho phép code tiếp tục được thực thi. Trong khi đợi một action khác được hoàn thành.
## Có bao nhiêu kiểu dữ liệu trong Javascript:
- Datatype primitives: 
  - Number: số
  - String: chuỗi
  - Boolean: true/false
  - Null: null
  - Undefined: undefined
  - Symbol: ký hiệu
  - Bigint: số lớn
- DataType Object:
  - Object: đối tượng
  - Array: mảng
  - Function: hàm
  - Date: ngày
  - RegExp: biểu thức chính quy
  - Error: lỗi
  - Promise: promise
***
## Hoisting là gì? Tìm hiểu về Execution context. Trình bày về var, let, const
Hoisting là hành động mặc định của Javascript, nó sẽ chuyển phần khai báo lên phía trên top Trong Javascript, một biến (variable) có thể được khai báo sau khi được sử dụng.
```js
console.log(a);
var a = 'Hello Hoisting'
#output: undefined
```
+ Trình biên dịch của Javascript sẽ phân tách phần ```var a = 'Hello Hoisting'``` thành 2 phần:
  - declare: var a
  - assignment: = 'Hello Hoisting'
+ Hoistring sẽ chuyển phần khai báo lên top. Vì thế sẽ chỉ có phần khai báo được chuyển lên top, còn phần gán sẽ chờ đợi đến khi được sử dụng.
> Vì thế, nếu muốn sử dụng biến trong phần khai báo, ta phải khai báo trước.
### Lưu ý:
- Hoisting với biến, và Hoisting với hàm sẽ tương tự nhau:
- Hoisting với biến:
  - Biến sẽ được tạo trước khi chạy chương trình.
- Hoistring với hàm:
  - Hàm sẽ được tạo trước khi chạy chương trình.
- Phần khai báo hàm được đưa lên trước phần khai báo của biến
> Vì vậy, nếu bạn muốn sử dụng hàm trong hàm khác, bạn phải gọi hàm trước khi gọi hàm khác.

### Execution context
  Mỗi function được gọi sẽ sinh ra một execution context mới.Mỗi excution context được xử lý sẽ có 2 giai đoạn:
  - Khởi tạo:
    - Create global excution context
    - Create global object, window in the browser, global in nodejs
    - Create this object and bind it to the global object
      - This lúc này đại diện cho windows / global
    - Set-up a memory heap for storing variables and functions referenced
      - Khởi tạo bộ nhớ cho biến và hàm được gọi
    - Gắn giá trị underfined cho các hàm và biến trong global context
  - Thực thi:
   - Execution line by line ( từng dòng một) và gán giá trị thực cho biến đã tồn tai trong memory

### var, let, const
  - var:
    - function scoped
  - let:
    - Block scoped
  - const:
    - Block scoped

***
## Hows Javascript works. Nắm được cách hoạt động của Javascript: memory, stack, heap, web apis, callback queue, event loop

- Memory heap, CallStack là 2 component chính của Engine Javascript V8.
  - memory heap: Thực hiện các tác vụ phân bổ vùng nhớ
  - Call stack:Nơi các stack frames trong code sẽ được thực thi.

![](https://images.viblo.asia/64a0844b-77f6-4611-9683-ce91f8b66c5e.png)


### Call stack:
 Javascript là một ngôn ngữ đơn luồng single-threaded, nó không có chức năng multi-threaded.Có nghĩa là nó chỉ có một call stack đơn lẻ. Cho nên nó chỉ thực hiện duy nhất mỗi 1 tác vụ cho một lần thực thi

 Call Stack là một cấu trúc dữ liệu ghi lại cụ thể nơi mà đoạn code của chúng ta được thực thi trong chương trình. Khi bạn bắt đầu một function, function đó sẽ được đẩy vào vùng chứa đầu tiên của stack. Rồi khi function đó kết thúc thì nó sẽ được remove ra khỏi stack. Đó là toàn bộ những gì stack có thể làm cho bạn.

 ```js
  function multiply(x, y) {
      return x * y;
  }
  function printSquare(x) {
      var s = multiply(x, x);
      console.log(s);
  }
  printSquare(5);
 ```
  “Blowing the stack” là một trường hợp nhỏ nhất của call stack. Nó xảy ra khi bạn gọi một function trong function khác.
  ```js  
    function foo() {
      foo();
    }
    foo();
  ```

### Event Loop:
- Event Loop có một nhiệm vụ duy nhất - giám sát Call Stack* và Callback Queue. Nếu Call Stack trống, nó sẽ lấy sự kiện đầu tiên từ queue và đẩy vào Call Stack


### Trình bày về scope ?
  Scope as The current context of excution.
  - scope : vùng có thể truy cấp dữ liệu
-> Mỗi khi gọi function, một new Execution context được tạo ra.Trong phase creation , sẽ tạo ra khung giao diện, gọi là global object.Và Excution phase ends thì Excution context sẽ pop ra khỏi stack.Chúng ta không thể truy cập được function của Excution Context đã bị pop ra khỏi stack.

Ex pop off Excution Context:
```js
function foo () {
  var bar = 'Declared in foo'
}

foo()

console.log(bar)
```
-> Khi mà trong function khai báo biến thì nó sẽ được gán vào scope của Excution context đó.
```js
function first () {
  var name = 'Jordyn'

  console.log(name)
}

function second () {
  var name = 'Jake'

  console.log(name)
}

console.log(name)
var name = 'Tyler'
first()
second()
console.log(name)
```
Khi mà Javascript engine không thể tìm được biến local function Context thì nó sẽ tìm tới Parent Excution Content gần nhất. Nếu không có thì sẽ tiếp tục tìm ở Global Context . Global Context không có variable . Nó sẽ trả về Reference Error.(lỗi tham chiếu).
```js
var name = 'Tyler'

function logName () {
  console.log(name)
}

logName()
```
> Trong trường hợp hàm gọi một lần , thì đó chính là Closure .
***

## Các loại function: Closures , arrow function, higher order functions…?
### Closures:
  - Là một hàm bên trong mà có thể truy cập biến của hàm bên ngoài. Có 3 scoped là :
    - Truy cập biến của chính nó
    - Truy cập biến của hàm bên ngoài
    - Truy cập biến cục bộ

### Emulating private methods with closures: *Mô phỏng private method với closure.(private method là một method không được truy cập bởi những function khác):*
  ```js
  var counter = (function() {
  var privateCounter = 0;
  function changeBy(val) {
    privateCounter += val;
  }

  return {
    increment: function() {
      changeBy(1);
    },

    decrement: function() {
      changeBy(-1);
    },

    value: function() {
      return privateCounter;
    }
  };
})();

  console.log(counter.value());  // 0.

  counter.increment();
  counter.increment();
  console.log(counter.value());  // 2.

  counter.decrement();
  console.log(counter.value());  // 1.
  ```
### Arrow Function : *Là một thay thế nhỏ gọn cho biểu thức hàm (function expression).Nhưng bị giới hạn và không thể dùng trong mọi tình huống.*
     Difference:
      - Không rằng buộc với this, super.
      - Không có new.target
      - Không có constructor
      - Không thể sử dụng yield
      - Không phù hợp cho call, apply, bind method. 

<h3>Các trường hợp không dùng:</h3>
1. Khi sử dụng với event handler ( this trong arrow function không được gán vào event handler, nó reference tới global object -> this(windows) -> không có thuộc tính value -> underfined )

  ```js
  const greeting = document.querySelector('#greeting');
  const username = document.querySelector('#username');
  username.addEventListener('keyup', () => {
    greeting.textContent = 'Hello ' + this.value;
  });
  ## this.value -> undefined
```
2. Object methods:
```js
const counter = {
  count: 0,
  next: () => ++this.count,
  current: () => this.count
};
```
- Higher order function: là một function nhận vào một hoặc nhiều function khác.
3. Prototype methods:
4. Functions that use the arguments object

## Prototypes và tầm quan trọng của prototypes trong JS?
Prototype là cơ chế mà các object trong javascript kế thừa các tính năng từ một object khác.

Có 2 cách khởi tạo: object litalization và object constructor.

```js
let person = {
  name: 'Jordyn',
  age: 24
};
## Object Literal
```

```js
function Person(name) {
  this.name = name;
}
let Han = new Person('Han');
## Object Constructor
``` 

Trong Prototype có 2 thuộc tính:
- Prototype: là một object được gán vào prototype của một object.
- Constructor: là một function được gán vào prototype của một object.
![](./image/prototype.png)

```js

function Person() {
  this.name = 'John';
  this.age = 30;
}
Person.prototype.status = 'single';
const person1 = new Person();


console.log('person1', person1);
```
Các đối tượng mới sẽ thay đổi giá trị thuộc tính nếu thay đổi giá trị nguyên mẫu. Tất cả các đối tượng trước đó sẽ có giá trị trước đó.
Khi chương trình được chạy thì person1.name sẽ xem trong hàm tạo có thuộc tính name và lấy giá giá trị là 'John', nhưng vì hàm tạo không có thuộc tính status nên nó sẽ nhìn vào đối tượng nguyên mẫu (prototype object) và kế thừa từ đó. 
> Nhưng tại sao lại phải dùng prototype object? Mà không tạo thẳng trong function contructor .

    Vì nếu tạo thẳng trong function contructor thì nó sẽ kế thừa từ prototype object của function contructor.Dẫn đến khi tạo đối tượng mới thì sẽ tạo object mới, không liên quan tới object đã tạo trước đó. 
```js
function Person(name) {
  this.name =  name;
  this.status = 'single';
}

const person1 = new Person();
const person2 = new Person();
console.log(person2.status === person1.status); // false
```
-> Lợi ích của prototype object: 
  - Tiết kiệm bộ nhớ 
  - Giảm thiểu số lần tạo đối tượng mới.
  - Giảm thiểu số lần gọi đến function contructor.
***
## ES6 array method?
### Một vài array method hay dùng ?
1. Array.prototype.map(): 
*The map() method creates a new array with the results of calling a provided function on every element in the calling array.*

    <strong>Khi nào không dùng ? </strong>
      - you're not using the array it returns; and/or
      - you're not returning a value from the callback.  
3. Array.prototype.reduce()
  *Giá trị được trả về từ chạy "reducer" callback function trên toàn bộ mảng.*
  ![](https://clojurebridgelondon.github.io/workshop/images/map-reduce-sandwich.png)
2. Array.prototype.filter():
  *The filter() method creates a new array with all elements that pass the test implemented by the provided function.*
  Nếu không có element nào vượt qua thì sẽ trả về mảng trống

4. Array.prototype.push()
  *The push() method adds new items to the end of an array, and returns the new length.*
5. Array.prototype.forEach()
  *The forEach() method executes a provided function once for each array element.*
  -> Chạy nhanh hơn cả for thường dùng để duyệt mảng.
6. Array.prototype.splice()
  *The splice() method changes the contents of an array by removing or replacing existing elements and/or adding new elements.*

  Syntax:
  ```js
    array.splice(start, deleteCount[, item1[, item2[, ...]]])
  ```

  - start : Vị trí bắt đầu thay đổi array.start > length -> start = length 
  - deleteCount : Số phần tử sẽ bị xóa. deleteCount > length -> deleteCount = length ( deleteCount = 0 || < 0  -> không xóa gì cả). Nếu không có deleteCount thì sẽ xóa từ start đến hết array.
  - item : Các phần tử mới sẽ thêm vào array.

7. Array.prototype.slice()
  *The slice() method returns a shallow copy of a portion of an array into a new array object selected from begin to end (end not included).Tương tự nhưng truy cập 1 phần của mảng và không thay đổi nó.*
***
## Trình bày về promises và async/await?
### Promise: *là đối tượng đại diện cho kết quả của hành động nào đó sẽ hoàn thành trong tương lai, kết quả trả về sẽ là resolve nếu thành công và reject nếu thất bại.*

Promise nó chạy trước khi bắt đầu event loop tiếp theo, còn setTimeOut thì sẽ chạy vào event loop tiếp theo.

- Pending: promise đang thực hiện chưa xong
- Full filled: trạng thái đã thực hiện xong, kết quả thành công
- Rejected: trạng thái đã thực hiện xong, kết quả thất bại
> Lưu ý:
- Promise là 1 object được tạo ra bởi một function.
- Promise sẽ ở 1 trong 3 trạng thái: pending, fulfilled, rejected.
- Promise có 3 hàm: then, catch, finally.
- Promise có 1 hàm: all,
- Promise có 2 lời hứa: resole, reject

*Promise Contructor method*: Promise.resolve(value), Promise.reject(reason).

> Tại sao lại dùng 2 method này: Để pass nhanh value từ then này sang then khác mà không cần phải return promise

  
### Async/await:  
Async/await làm cho code bất đồng bộ trở nên có vẻ giống đồng bộ, do đó dễ đọc hơn. Tuy nhiên, chỉ nên dùng khi biết rõ về nó, có những trường hợp không thể dùng async/await được.

Lưu ý:
  - return a promise
  - Chỉ có thể dùng trong function async.Không thể dùng trong global scope.
  - Viết code bất đồng bộ như cách viết code đồng bộ 
  - Khi dùng async await hãy chắc chắn dùng try catch để bắt lỗi.
  - Cẩn thận khi dùng await trong loop. Có thể rơi vào mã thực thi liên tục khi mà có thể dễ dàng thực hiện song song.
  - Cẩn thận khi dùng await trong callback.

### Nên dùng Promise hay Async await ?

> Nên dùng cả hai :
  - Nếu output của function2 phụ thuộc vào function1, hãy dùng async
  - Để run Promise song song, thì dùng Promise.all
  - Tránh viết quá nhiều await Function trong async function.Hãy khiến nó nhỏ nhất có thể.
  - Async await thực thi code bất đồng bộ, như cách viết code đồng bộ.Nên await sẽ chặn các lệnh phía sau để chờ một promise nào đó hoàn thành. Vì vậy, chỉ khi bạn thực sự biết await sẽ làm gì thì mới dung, nếu không có thể nó sẽ gấy blocking.
  - Trình duyệt cũ không hỗ trợ, cần dùng thêm babel để chuyển async await sang promise.

## Trình bày React Component ?
React định nghĩa components như class hoặc function.
1. Class Components:
```js
class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}
```
2. Function Components:
```js
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}
```
3. Stateless Functional Components:
```js
const Welcome = (props) => <h1>Hello, {props.name}</h1>;
```
Khi HOOK được sinh ra trong React, thì giúp cho React viết theo Function Components.Trong Components thì 2 cái quan 
### Component Life Cycle:
- Mounting: component được render lên DOM
- Updating: component được render lại
- Unmounting: component được unmount ( remove khỏi DOM)
***
## Trình bày về react hook? useState, useEffect, useCallback, useRef?
Chỉ có thể được gọi trong function component.
- Có một instance của hook được tạo ra cho mỗi component.Chúng chỉ là JavaScript object.Chúng ta có thể put data . Khi gọi một Hook như useState, nó sẽ đọc curent call, sau đó 
- Khi nào thì dùng hook : viết function component , và cần add một vài state tới nó.
### Nguyên tắc sử dụng :
Luôn gọi hook ở trên cùng trước bất kì việc trả về return nào. Không gọi trong loop, câu điều kiện hay các function lồng với nhau ( có thể sử dụng ESlint để đảm bảo các quy tắc này).
1. useState:
```js
const [count, setCount] = useState(0);
```
Input: initialState (giá trị hoặc function)

Output: một mảng có 2 phần tử tương ứng cho state và setState.

Những lưu ý: 
useState use Replacing instead of MEARING.
```js
const [person, setPerson] = useState({
  name: '',
  age: 0
});

setPerson({...person,  age: 0});
```

2. useEffect:
```js
useEffect(() => {
  document.title = `You clicked ${count} times`;
});
```
What does useEffect do? 
Khi sử dụng hook thì bạn nói với React rằng component của bạn cần làm gì đó sau khi render. React sẽ nhớ function bạn truyền vào,và gọi lại nó sau khi DOM updates.

Why is useEffect called inside a component?
- Để chúng ta truy cập variable state của component. Không cần một API đặc biệt để đọc nó, nó đã nằm trong scope của function. Hook tận dụng JS closure
Does useEffect run after every render
Mặc định nó sẽ chạy cả sau khi render lần đầu tiên và sau mỗi lần update. 

>Return cleanup
useEffect cho phép return 1 function, function này sẽ thực thi trước khi mà component đó được Unmount.Vì là effect trên tất cả những lần render . Do đó React đồng thời clean up effect từ những lần render trước.
-> Để tối ưu Performance, truyền một array vào useEffect.
```js
useEffect(() => {
  document.title = `You clicked ${count} times`;
}, [count]);
```
Nếu count được update thì useEffect sẽ được gọi lại.
3. useCallback:
```js
const [count, setCount] = useState(0);
const increment = useCallback(() => setCount(count + 1), [count]);
```
-> Trả về một callback đã ghi nhớ.useCallback sẽ trả về một bản ghi nhớ của callback mà nó chỉ thay đổi khi ít nhất 1 phụ thuộc thay đổi. Hay dụng để tránh component con được render lại khi nó không thay đổi.
4. useRef:
```js
const [count, setCount] = useState(0);
const inputEl = useRef(null);
```
useRef trả về một đối tượng ref có thể thay đổi nơi mà thuộc tính .current được khởi tạo và thêm vào giá trị của (initialValue)
Về bản chất thì useRef giống như một cái "hôp" ,à có thể lưu giữ một giá trị có thể thay đổi bên trong .current property của nó. 
Thay đổi thuộc tính .current sẽ không re-render. 

## Các cách quản lý State trong React

Trường hợp 1: State chỉ được dùng trong 1 component và không vấn đề gì khi truyền props xuông cho component con với 2-3 cấp thì dùng local state (useState)

Trường hợp 2: Truyền cho quá nhiều con ( 4-5 cấp) và các con này nằm bên ngoài hay bên trong cha của nó thì có thể sử dụng Redux hoặc react Context
### React Context API:
- React Context API là một API được sử dụng để truyền dữ liệu từ component cha xuống component con.
Lý do sinh ra : để tránh việc truyền dữ liệu qua nhiều lớp. Ví dụ thông thường các biến state sẽ đặt ở component cha, nhưng nếu có nhiều component con thì các component con sẽ phải truyền dữ liệu qua nhiều lớp. (A -> B -> C | C-> B->A).

![](./image/context-api.png)

Ưu điểm : hạn chế lặp lại code, hạn chế truyền props không cần thiết và quản lý state dễ dàng hơn.
Nhược điểm : Khó tái sử dụng component vì state tập trung lại một chỗ, tương tự như việc sử dụng biến toàn cục.

### Redux:
Nguyên tắc : store là đối tượng duy nhất của ứng dụng, nó chứa toàn bộ state của ứng dụng.Cập nhật dữ liệu bằng cách thực hiện một action để cho biến dữ liệu sẽ thay đổi như nào, sau đó sẽ diễn giải các hành động và cập nhật dữ liệu mới với các reducer. Reducer sẽ trả về 1 state mới, thay vì thay đổi state trước đó.

Trường hợp 3:
- Truyền state cho nhiều con mà các con này nằm bên trong cha của nó. Thì có thể dùng cách component composition (đẩy state lên component cha và truyền xuống con sử dụng {props.children}).


### React-Query:
- React-Query là một library được sử dụng để tạo ra các query để lấy dữ liệu từ API.
-> Chia thành client state và server state.
--------------------------------------------------------------------------
# Tài liệu tham khảo
- [Bất đồng bộ trong Javascript](https://viblo.asia/p/bat-dong-bo-trong-javascript-maGK7J8D5j2)
- [Mô phỏng event loop trong javascript](http://latentflip.com/loupe/?code=JC5vbignYnV0dG9uJywgJ2NsaWNrJywgZnVuY3Rpb24gb25DbGljaygpIHsKICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gdGltZXIoKSB7CiAgICAgICAgY29uc29sZS5sb2coJ1lvdSBjbGlja2VkIHRoZSBidXR0b24hJyk7ICAgIAogICAgfSwgMjAwMCk7Cn0pOwoKY29uc29sZS5sb2coIkhpISIpOwoKc2V0VGltZW91dChmdW5jdGlvbiB0aW1lb3V0KCkgewogICAgY29uc29sZS5sb2coIkNsaWNrIHRoZSBidXR0b24hIik7Cn0sIDUwMDApOwoKY29uc29sZS5sb2coIldlbGNvbWUgdG8gbG91cGUuIik7!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D)
- [Hoisting, Scopes, and Closures in JavaScript ](https://ui.dev/ultimate-guide-to-execution-contexts-hoisting-scopes-and-closures-in-javascript)
- [JavaScript Visualizer](https://ui.dev/javascript-visualizer?code=var%20name%20%3D%20%27Tyler%27%0Avar%20handle%20%3D%20%27%40tylermcginnis%27%0A%0Afunction%20getUser%20%28%29%20%7B%0A%20%20return%20%7B%0A%20%20%20%20name%3A%20name%2C%0A%20%20%20%20handle%3A%20handle%0A%20%20%7D%0A%7D)