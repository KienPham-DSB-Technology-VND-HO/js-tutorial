import fetch from 'node-fetch';

function getFetchApi(name) {
  return fetch(`https://jsonplaceholder.typicode.com/posts/${name}`)
}

console.time('time_promise_all');
function testSpeed() {
  const data1 = getFetchApi('1');
  const data2 = getFetchApi('2');
  const data3 = getFetchApi('3');
  console.timeEnd(`time_promise_all`);
  return Promise.all([data1, data2, data3]);
}
  
testSpeed();


console.time(`time_async`);
const testAsyncSpeed = async () => {
  const data1 = await getFetchApi('1');
  const data2 = await getFetchApi('2');
  const data3 = await getFetchApi('3');
  console.timeEnd(`time_async`);
  return [data1, data2, data3];
}
testAsyncSpeed();


console.time(`time_promise`);

function testSpeedPromise() {
  const arr = [];
  getFetchApi('1').then(data1 => {
    return arr.push(data1);
  }).then(() => {
    getFetchApi('2').then(data2 => {
      return arr.push(data2);
    }).then(() => {
      getFetchApi('3').then(data3 => {
        return arr.push(data3);
      }).then(() => {
        console.timeEnd(`time_promise`);
      })
    })
  }
  )
}
testSpeedPromise();

/* 
  - Nếu mà các function sau không phụ thuộc vào value của function trước đó thì hãy sử dụng Promise.all
  - Tại sao async thì lại nhanh hơn promise?
    -> Mỗi khi await thì nó sẽ chờ , execute xong thì nó sẽ chạy đến cái tiếp theo. Thay vì nó sẽ chạy qua một loạt rồi ném hết đồng promise vào callback Queue.
*/