const total = 1000000;

const arr = Array(total);

console.time('for');
for (let i = 0; i < total; i++) {
}
console.timeEnd('for');

console.time('forEach');
arr.forEach(() => {

})
console.timeEnd('forEach');

console.time('for_of');
for(let item of arr) {

}
console.timeEnd('for_of');


let numbers = [1, 2, 3, 4, 5];
//map
numbers = numbers.map(v => v * 3);
console.log({numbers});
//filter

numbers = numbers.filter(v => v % 2 === 0);
console.log({numbers});
//reduce
let sum = numbers.reduce((sum, v) => sum += v);

console.log({ sum });

// Loop với Object :

const myObject = { name: 'Han', age: 22 }

for (const item of Object.entries(myObject)) {
    console.log(item);
}