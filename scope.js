// Trường hợp khai báo biến trong function
function first() {
  var name = 'Jordyn'

  console.log(name)
}

function second() {
  var name = 'Jake'

  console.log(name)
}

console.log(name)
var name = 'Tyler'
first()
second()
console.log(name)  // ?


// Trường hợp khai báo biến ngoài function
var name = 'Tyler'

function logName() {
  console.log(name)
}

logName() // Tại sao ở trường hợp này có thể log ra được value của name? 


// Global Variables

// In the browser
var name = 'Tyler'

function foo() {
  bar = 'Created in foo without declaration'
}

foo()

console.log(window.name) // Tyler
console.log(window.bar) // Created in foo without declaration

/* Khi tạo 1 biến mà không khai báo var, let, const thì biến đó sẽ được add trong global object.
Thử nghiệm trên node v16 thì xẩy ra lỗi is not define 
*/